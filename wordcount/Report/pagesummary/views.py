from django.shortcuts import render, render_to_response
from django.template import RequestContext
from logic import grab_page, parse


from .models import Report, MetaTags, CommonWords
from .forms import GetPageForm

# Create your views here.

def search(request):
    #validate = URLValidator(verify_exists=True)
    context = RequestContext(request)
    if request.method == "POST":
        form = GetPageForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data['url']
            page = grab_page(url)
            return resultsPage(request, page)
        else:
            print form.e
    else:
        form = GetPageForm()

    return render_to_response('index.html', {'form':form}, context)


def resultsPage(request, url):
    context = RequestContext(request)
    return render_to_response('results.html', locals(), context)
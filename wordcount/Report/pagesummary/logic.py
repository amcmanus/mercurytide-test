__author__ = 'andrew'


import urllib2
from bs4 import BeautifulSoup
from .models import MetaTags, CommonWords, Report


def grab_page(url):

    page = urllib2.Request(url)
    response = urllib2.urlopen(page)
    result = response.read()
    return result

def parse(html_doc, url):
    soup = BeautifulSoup(html_doc)
    title = soup.title.name
    meta_tags = soup.find_all('meta')
    a_tags = soup.find_all('a')
    wordcount = len(soup.get_text())

    uniquelist = []
    commonlist = {}
    for word in soup.get_text():
        if word not in uniquelist:
            uniquelist.append(word)
        if word not in commonlist:
            commonlist[word] = 1
        elif word in commonlist:
            commonlist[word] += 1

    uniquecount = len(uniquelist)
    commonlistout = sorted(commonlist, key=commonlist.get, reverse=True)[:5]

    r = Report(title=title, url=url, uniquewords = uniquecount, filesize=0, wordcount = wordcount)
    r.save()

    for m in meta_tags:
        m1 = MetaTags(title=m)
        m1.save()
        r.metatags.add(m1)
        r.save()

    for c in commonlist:
        c1 = CommonWords(title=c)
        c1.save()
        r.commonwords.add(c1)
        r.save()

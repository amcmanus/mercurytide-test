from django.db import models

# Create your models here.

# Word Count
# Number of unique words
# Most common 5 words
# page title
# meta tags
# file size


class MetaTags(models.Model):
    tag = models.CharField(max_length=100)


class CommonWords(models.Model):
    word = models.CharField(max_length=100)


class HrefTags(models.Model):
    source = models.URLField()
    text = models.CharField(max_length=100)


class Report(models.Model):
    title = models.CharField(max_length=100)
    url = models.URLField()
    uniquewords = models.IntegerField()
    filesize = models.IntegerField() #in KB
    metatags = models.ManyToManyField(MetaTags)
    wordcount = models.IntegerField()
    commonwords = models.ManyToManyField(CommonWords)




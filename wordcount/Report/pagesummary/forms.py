__author__ = 'andrew'

from django import forms
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from .models import Report, CommonWords, MetaTags


class GetPageForm(forms.Form):
    validate = URLValidator()
    url = forms.URLField()
    try:
        validate(url)
    except ValidationError, e:
        print e
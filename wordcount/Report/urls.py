from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Report.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'Report.pagesummary.views.search', name='search'),
    url(r'^result/$', 'Report.pagesummary.views.resultsPage', name='result'),


    url(r'^admin/', include(admin.site.urls)),
)
